package Reqresin.DzTests;

import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;

public class PutSingleUserTest {
    @Test
    @DisplayName("Вызов метода PUT/Update User. Обновление данных для пользователя")
    public void successPutSingleUser() throws Exception {
        byte[] out = "{\"last_name\": \"Hilty\"}".getBytes(StandardCharsets.UTF_8);

        HttpRequest request = HttpRequest.newBuilder()
                .uri(new URI("https://reqres.in/api/users/3"))
                .PUT(HttpRequest.BodyPublishers.ofByteArray(out))
                .build();

        HttpResponse response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
        System.out.println(response.body());
        System.out.println(response.statusCode());
    }

}

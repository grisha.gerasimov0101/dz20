package Reqresin.DzTests;

import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class DeleteSingleUserTest {
    @Test
    @DisplayName("Вызов метода DELETE/Delete User. Удаление пользователя")
    public void successDeleteSingleUser() throws Exception {

        HttpRequest request = HttpRequest.newBuilder()
                .uri(new URI("https://reqres.in/api/users/6"))
                .DELETE().build();

        HttpResponse response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
        System.out.println(response.statusCode());
    }
}

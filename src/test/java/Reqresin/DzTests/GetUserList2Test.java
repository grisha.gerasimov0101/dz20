package Reqresin.DzTests;

import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GetUserList2Test {
    @Test
    @DisplayName("2 вызов метода GET/UserList. Получение списка пользователей")
    public void successGetUserList2() throws Exception {
        HttpRequest request = HttpRequest.newBuilder()
                .uri(new URI("https://reqres.in/api/users?page=1"))
                .GET().build();

        HttpResponse response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
        System.out.println(response.statusCode());
        System.out.println(response.body());

        assertEquals(200, response.statusCode(), "Статус код не соответствует документации");
    }
}
